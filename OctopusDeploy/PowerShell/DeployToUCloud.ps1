[System.Reflection.Assembly]::LoadWithPartialName("System.Web")

function Test-LastExit($cmd) {
    if ($LastExitCode -ne 0) {
        Write-Host "##octopus[stderr-error]"
        write-error "$cmd failed with exit code: $LastExitCode"
    }
}

$tempDirectoryPath = (Get-Item -Path ".\" -Verbose).FullName
Write-Host "tempDirPath: $tempDirectoryPath"
$tempDirectoryPath = join-path $tempDirectoryPath "GitPush" 
Write-Host "tempDirPath: $tempDirectoryPath"
$tempDirectoryPath = join-path $tempDirectoryPath $OctopusParameters['Octopus.Environment.Name']
Write-Host "tempDirPath: $tempDirectoryPath"
$tempDirectoryPath = join-path $tempDirectoryPath $OctopusParameters['Octopus.Project.Name']
Write-Host "tempDirPath: $tempDirectoryPath"
$tempDirectoryPath = join-path $tempDirectoryPath $OctopusParameters['Octopus.Action.Name']

Write-Host "tempDirPath: $tempDirectoryPath"

$stepName = $PackageStepName  ##should be "UnpackNuget"

Write-Host $stepName + " - should be UnpackNuget"

Write-Host "Repository will be cloned to: $tempDirectoryPath"

# Step 1: Ensure we have the latest version of the repository
mkdir $tempDirectoryPath -ErrorAction SilentlyContinue
cd $tempDirectoryPath

Write-Host "##octopus[stderr-progress]"
 
 ## need to add an alias so we can use "git"
new-item -path alias:git -value 'C:\Program Files\Git\bin\git.exe'
 
$branch = $GitBranch

#Construct the UaaS git url with credentials
$UsernameEncoded = [System.Web.HttpUtility]::UrlEncode("$GitUsername")
$PasswordEncoded = [System.Web.HttpUtility]::UrlEncode("$GitPassword")

$currentRemoteUri = New-Object System.Uri "$GitCloneUrl"

$newRemoteUrlBuilder = New-Object System.UriBuilder($currentRemoteUri)
$newRemoteUrlBuilder.UserName = $UsernameEncoded 
$newRemoteUrlBuilder.Password = $PasswordEncoded

## clone remote repo
git clone $newRemoteUrlBuilder.ToString()

## the Extracted Fle Location from the previous step
$stepPath = ""
$stepPath = $OctopusParameters["Octopus.Action[UnpackNuget].Output.ExtractedFileLocation"]

Write-Host "Nuget Package was extracted to: $stepPath"

#Copy the buildout to the UaaS repository excluding the umbraco and umbraco_client folders, and some other stuff
Write-Host "Copying Build output to the UaaS repository..."

$dataFolder = $stepPath
$targetDataFolder = $tempDirectoryPath + "\$GitRepoNameGuid"
Write-Host "From: $dataFolder  To: $targetDataFolder"
Get-ChildItem -Path $dataFolder | % { Copy-Item $_.fullname "$targetDataFolder" -Recurse -Force -Exclude @("umbraco", "umbraco_client", "package", "*.zip", "*.nuspec") }

#Change location to the Path where the UaaS repository is cloned
## need to switch one level down for the cloned repo - created using the UaaS git repo "guid"
$localGitPath = $tempDirectoryPath + "\$GitRepoNameGuid"
Set-Location -Path $localGitPath

Write-Host "Local git path is: $localGitPath" 

#Silence warnings about LF/CRLF
Write-Host "Silence warnings about LF/CRLF and add git user name"
git config core.safecrlf false
git config user.email "$GitUsername"
git config user.name "$GitUsername"

# Step 3: Push the results
$deploymentName = $OctopusParameters['Octopus.Deployment.Name']
$releaseName = $OctopusParameters['Octopus.Release.Number']
$projName = $OctopusParameters['Octopus.Project.Name']

Write-Host "Deployment Name: $deploymentName"
Write-Host "Release Name: $releaseName"
Write-Host "Project Name: $projName"

#Commit the build output to the UaaS repository
Write-Host "Current status of the UaaS repository"
git status
git add -A --force
Write-Host "Status after add: "
git status
git commit -m "$projName release $releaseName - $deploymentName"
Write-Host "Status after commit: "
git status

#git push up all the changes
git push $newRemoteUrlBuilder.ToString() $branch
Write-Host "Status after push:"
git status
Test-LastExit "git push"