$PackageLocation = "#{NugetPackageLocation}" #Location of Octo Packages 
$ExtractionLocation = "#{ExtractionLocation}"
$ThisPath = (Get-Item -Path ".\" -Verbose).FullName #Current working directory

## Output variable to be used in next step so we know where to find the files
Set-OctopusVariable -name "ExtractedFileLocation" -value $ExtractionLocation

Write-Output "File Location: #{NugetPackageLocation}"
Write-Output "Extracted File Location: $ExtractionLocation"

#This function unpacks a zipped file (which is all nuget packages are, zips with extra metadata)
function Expand-ZIPFile($file, $destination)
{
    $shell = New-Object -ComObject Shell.Application
    $zip = $shell.NameSpace($file)
    foreach($item in $zip.items())
    {
        $shell.Namespace($destination).copyhere($item)
    }
}

function UnZip($filename, $dest)
{
$shell_app=new-object -com shell.application

$FOF_SILENT_FLAG = 4
$FOF_NOCONFIRMATION_FLAG = 16

if (Test-Path $filename)
{
   Write-Host Unzipping $filename
   $zip_file = $shell_app.namespace("$filename")
   $destination = $shell_app.namespace("$dest")
   $destination.Copyhere($zip_file.items(), $FOF_SILENT_FLAG + $FOF_NOCONFIRMATION_FLAG)
}
else
{
    Write-Host File $filename does not exist
}
}

$File = gci $PackageLocation | sort LastWriteTime | select -last 1

$FilePkgLocation = $File.FullName

Write-Output "Package Location: $PackageLocation"
Write-Output "Path: $ExtractionLocation"
Write-Output "File: $File"
Write-Output "File Package: $FilePkgLocation"

#Set some file variables
$FileName = $File.Name
$FileName = $FileName -replace "nupkg", "zip" 
$FileLocation = "$($ExtractionLocation)\$($FileName)"

## remove all files/folders in the /work/ folder
if (Test-Path $ExtractionLocation)
{
    Write-Host "Removing files"
    Remove-Item $ExtractionLocation -recurse
}

Write-Host "Creating working directory"
New-Item $ExtractionLocation -type directory -force


## copy the nuget package and rename to *.zip
Copy-Item "$($FilePkgLocation)" -Destination $FileLocation -Recurse

##Dir *.nupkg | rename-item -newname {  $_.name  -replace ".nupkg",".zip"  } #Rename the nupkg to a zip

##Expand-ZIPFile -File $FileLocation -Destination $ThisPath #Call our unzip method into the Nuget folder
Unzip -filename $FileLocation -dest $ExtractionLocation

Get-ChildItem ./ -Force

Remove-Item "$ExtractionLocation\_rels" -Force -Recurse #Remove this weird rels file