# OctopusDeploy Configuration #

This approach uses a standard OctopusDeploy configuration with a two-step process defined.  Each step relies on a custom step template which uses the Powershell scripts provided, and has parameters defined for items such as user and password.  Additonal steps may be configured as needed - we typically add a step to notify a Slack channel of the deployment status when complete.

## Process ##
### Step 1 - Unpack Nuget ###

Define the step as using a custom step template.  To that step template add the `ExtractNugetPackage.ps1` script.  Add two parameters to the step template:

* NugetLocation - `#{NugetPackageLocation}`
* Nuget Extraction Location - `#{ExtractionLocation}`

In the process step set the value of the parameters to the location where the Nuget package will be placed by the TeamCity build and the location where the Nuget package will be extracted.  This extraction location is used by the next step to locate the extracted files.

![ExtractNuget](https://bytebucket.org/proworks/clouddeploy/raw/ed1b19a2249df3d02e72fb1d9c8fae49b91b6dd0/Documentation/Images/OctopusDeployUnpackNuget.PNG "ExtractNugetStep")

Figure 1 

### Step 2 - Push to Umbraco Cloud ###

Define the step as using a custom step template.  To that step template add the `DeployToUCloud.ps1` script.  Add six parameters to the step template:

* Clone Url - `#{GitCloneUrl}`
* Git Username - `#{GitUsername}`
* Git Password - `#{GitPassword}`
* Git Branch Name = `#{GitBranch} = master`
* Previous Step Name - `#{PreviousStepName}`
* Git Repo Name Guid - `#{GitRepoNameGuid}`

In the process step set the value of the parameters to the values from your Umbraco Cloud project and the previous step name.

![DeployToUCloud](https://bytebucket.org/proworks/clouddeploy/raw/ed1b19a2249df3d02e72fb1d9c8fae49b91b6dd0/Documentation/Images/OctopusDeployDeployToUCloud.PNG "DeployToUCloudStep")

Figure 2

