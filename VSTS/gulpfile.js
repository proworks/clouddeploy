var gulp = require('gulp'),
    watch = require('gulp-watch');

gulp.task('views', function () {

    gulp.src('./Views/**/*.cshtml')
        .pipe(debug({ title: 'copy view:' }))
        .pipe(gulp.dest('../YourProjectName.Web/Views'));
});

gulp.task('images', function () {

    gulp.src('./images/*.*')
        .pipe(debug({ title: 'copy image:' }))
        .pipe(gulp.dest('../YourProjectName.Web/images'));
});

gulp.task('app-plugins', function () {

    gulp.src('./App_Plugins/**/*.*')
        .pipe(debug({ title: 'copy plugin file:' }))
        .pipe(gulp.dest('../YourProjectName.Web/App_Plugins'));
});

gulp.task('fonts', function () {

    gulp.src('./fonts/*.*')
        .pipe(debug({ title: 'copy font file:' }))
        .pipe(gulp.dest('../YourProjectName.Web/fonts'));
});

gulp.task('transforms', function () {

    gulp.src('web.*.xdt.config')
        .pipe(debug({ title: 'copy config transform files:' }))
        .pipe(gulp.dest('../YourProjectName.Web'));
});

gulp.task('watch', function () {
    watch('./css/**/*.scss', function () {
        gulp.start('sass');
    });
    watch(['./scripts/site/**/*.js'], function () {
        gulp.start('scripts');
    });
    watch(['./Views/**/*.cshtml'], function () {
        gulp.start('views');
    });
    watch(['./images/*.*'], function () {
        gulp.start('images');
    });
    watch(['./App_Plugins/**/*.*'], function () {
        gulp.start('app-plugins');
    });
    watch(['./fonts/*.*'], function () {
        gulp.start('fonts');
    });
    watch(['web.*.xdt.config'], function () {
        gulp.start('transforms');
    });
});