# VSTS Configuration #
The setup for this approach is covered in detail in the May 2018 Skrift.io article (link coming when published).

## Useful Files ##
You can find the gulp `watch` task noted in the above to help keep your `.Core` project up to date with files created or edited in the `.Web` project including `.uda` and `.courier` files created by Umbraco Deploy.